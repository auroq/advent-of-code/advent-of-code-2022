package challenges

import (
	d01p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day01/part1"
	d01p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day01/part2"
	d02p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day02/part1"
	d02p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day02/part2"
	d03p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day03/part1"
	d03p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day03/part2"
	d04p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day04/part1"
	d04p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day04/part2"
	d05p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day05/part1"
	d05p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day05/part2"
	d06p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day06/part1"
	d06p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day06/part2"
	d07p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day07/part1"
	d07p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day07/part2"
	d08p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day08/part1"
	d08p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day08/part2"
	d09p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day09/part1"
	d09p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day09/part2"
	d10p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day10/part1"
	d10p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day10/part2"
	d11p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day11/part1"
	d11p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day11/part2"
	d12p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day12/part1"
	d12p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day12/part2"
	d13p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day13/part1"
	d13p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day13/part2"
	d14p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day14/part1"
	d14p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day14/part2"
	d15p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day15/part1"
	d15p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day15/part2"
	d16p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day16/part1"
	d16p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day16/part2"
	d17p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day17/part1"
	d17p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day17/part2"
	d18p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day18/part1"
	d18p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day18/part2"
	d19p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day19/part1"
	d19p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day19/part2"
	d20p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day20/part1"
	d20p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day20/part2"
	d21p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day21/part1"
	d21p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day21/part2"
	d22p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day22/part1"
	d22p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day22/part2"
	d23p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day23/part1"
	d23p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day23/part2"
	d24p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day24/part1"
	d24p02 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day24/part2"
	d25p01 "gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/challenges/day25/part1"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/structure"
)

func GetDays() []structure.Day {
	return []structure.Day{
		structure.NewDay(1, d01p01.Part1{}, d01p02.Part2{}),
		structure.NewDay(2, d02p01.Part1{}, d02p02.Part2{}),
		structure.NewDay(3, d03p01.Part1{}, d03p02.Part2{}),
		structure.NewDay(4, d04p01.Part1{}, d04p02.Part2{}),
		structure.NewDay(5, d05p01.Part1{}, d05p02.Part2{}),
		structure.NewDay(6, d06p01.Part1{}, d06p02.Part2{}),
		structure.NewDay(7, d07p01.Part1{}, d07p02.Part2{}),
		structure.NewDay(8, d08p01.Part1{}, d08p02.Part2{}),
		structure.NewDay(9, d09p01.Part1{}, d09p02.Part2{}),
		structure.NewDay(10, d10p01.Part1{}, d10p02.Part2{}),
		structure.NewDay(11, d11p01.Part1{}, d11p02.Part2{}),
		structure.NewDay(12, d12p01.Part1{}, d12p02.Part2{}),
		structure.NewDay(13, d13p01.Part1{}, d13p02.Part2{}),
		structure.NewDay(14, d14p01.Part1{}, d14p02.Part2{}),
		structure.NewDay(15, d15p01.Part1{}, d15p02.Part2{}),
		structure.NewDay(16, d16p01.Part1{}, d16p02.Part2{}),
		structure.NewDay(17, d17p01.Part1{}, d17p02.Part2{}),
		structure.NewDay(18, d18p01.Part1{}, d18p02.Part2{}),
		structure.NewDay(19, d19p01.Part1{}, d19p02.Part2{}),
		structure.NewDay(20, d20p01.Part1{}, d20p02.Part2{}),
		structure.NewDay(21, d21p01.Part1{}, d21p02.Part2{}),
		structure.NewDay(22, d22p01.Part1{}, d22p02.Part2{}),
		structure.NewDay(23, d23p01.Part1{}, d23p02.Part2{}),
		structure.NewDay(24, d24p01.Part1{}, d24p02.Part2{}),
		structure.NewDay(25, d25p01.Part1{}, nil),
	}
}
