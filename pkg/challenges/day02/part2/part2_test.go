package part2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"A Y",
		"B X",
		"C Z",
	}
	expected := 12
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestScore(t *testing.T) {
	tests := []struct {
		name     string
		choice   Choice
		result   Result
		expected int
	}{
		{"win-paper", Paper, Win, 8},
		{"loss-rock", Rock, Loss, 1},
		{"draw-scissors", Scissors, Draw, 6},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			expected := test.expected
			actual := score(test.choice, test.result)

			assert.Equal(t, expected, actual)
		})
	}
}

func TestPlay(t *testing.T) {
	tests := []struct {
		name     string
		opponent Choice
		choice   Choice
		expected Result
	}{
		{"rock-paper", Rock, Paper, Win},
		{"paper-rock", Paper, Rock, Loss},
		{"scissors-scissors", Scissors, Scissors, Draw},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			expected := test.expected
			actual := play(test.opponent, test.choice)

			assert.Equal(t, expected, actual)
		})
	}
}

func TestParseLine(t *testing.T) {
	tests := []struct {
		line             string
		expectedOpponent Choice
		expectedResult   Result
	}{
		{"A Y", Rock, Draw},
		{"B X", Paper, Loss},
		{"C Z", Scissors, Win},
	}
	for _, test := range tests {
		t.Run(test.line, func(t *testing.T) {
			actualOpponent, actualResult := parse(test.line)

			assert.Equal(t, test.expectedOpponent, actualOpponent)
			assert.Equal(t, test.expectedResult, actualResult)
		})
	}
}

func TestGetChoice(t *testing.T) {
	tests := []struct {
		name           string
		opponent       Choice
		result         Result
		expectedChoice Choice
	}{
		{"A Y", Rock, Draw, Rock},
		{"B X", Paper, Loss, Rock},
		{"C Z", Scissors, Win, Rock},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actualChoice := getChoice(test.opponent, test.result)

			assert.Equal(t, test.expectedChoice, actualChoice)
		})
	}
}
