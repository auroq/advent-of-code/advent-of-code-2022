package part2

import (
	"strings"
)

type Part2 struct{}

type Choice int

const (
	Rock     Choice = 1
	Paper    Choice = 2
	Scissors Choice = 3
)

type Result int

const (
	Loss Result = 0
	Draw Result = 3
	Win  Result = 6
)

var choiceMappings = map[string]Choice{
	"A": Rock,
	"B": Paper,
	"C": Scissors,
}

var resultMappings = map[string]Result{
	"X": Loss,
	"Y": Draw,
	"Z": Win,
}

func (part Part2) Run(input []string) (answer int, err error) {
	for _, line := range input {
		opponent, result := parse(line)
		choice := getChoice(opponent, result)
		answer += score(choice, result)
	}

	return
}

func parse(line string) (opponent Choice, result Result) {
	vals := strings.Split(line, " ")
	opponent = choiceMappings[vals[0]]
	result = resultMappings[vals[1]]

	return
}

func play(opponent, choice Choice) Result {
	if opponent == choice {
		return Draw
	}
	switch choice {
	case Rock:
		if opponent == Scissors {
			return Win
		} else {
			return Loss
		}
	case Paper:
		if opponent == Rock {
			return Win
		} else {
			return Loss
		}
	case Scissors:
		if opponent == Paper {
			return Win
		} else {
			return Loss
		}
	}

	return Draw
}

func score(choice Choice, result Result) int {
	return int(choice) + int(result)
}

func getChoice(opponent Choice, result Result) Choice {
	if result == Draw {
		return opponent
	}
	switch opponent {
	case Rock:
		if result == Win {
			return Paper
		} else {
			return Scissors
		}
	case Paper:
		if result == Win {
			return Scissors
		} else {
			return Rock
		}
	case Scissors:
		if result == Win {
			return Rock
		} else {
			return Paper
		}
	}

	return opponent
}
