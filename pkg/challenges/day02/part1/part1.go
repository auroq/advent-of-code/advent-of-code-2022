package part1

import (
	"strings"
)

type Part1 struct{}

type Choice int

const (
	Rock     Choice = 1
	Paper    Choice = 2
	Scissors Choice = 3
)

type Result int

const (
	Loss Result = 0
	Draw Result = 3
	Win  Result = 6
)

var mappings = map[string]Choice{
	"A": Rock,
	"B": Paper,
	"C": Scissors,
	"X": Rock,
	"Y": Paper,
	"Z": Scissors,
}

func (part Part1) Run(input []string) (answer int, err error) {
	for _, line := range input {
		opponent, choice := parse(line)
		result := play(opponent, choice)
		answer += score(choice, result)
	}

	return
}

func parse(line string) (opponent, choice Choice) {
	vals := strings.Split(line, " ")
	opponent = mappings[vals[0]]
	choice = mappings[vals[1]]

	return
}

func play(opponent, choice Choice) Result {
	if opponent == choice {
		return Draw
	}
	switch choice {
	case Rock:
		if opponent == Scissors {
			return Win
		} else {
			return Loss
		}
	case Paper:
		if opponent == Rock {
			return Win
		} else {
			return Loss
		}
	case Scissors:
		if opponent == Paper {
			return Win
		} else {
			return Loss
		}
	}

	return Draw
}

func score(choice Choice, result Result) int {
	return int(choice) + int(result)
}
