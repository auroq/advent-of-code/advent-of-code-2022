package part1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"2-4,6-8",
		"2-3,4-5",
		"5-7,7-9",
		"2-8,3-7",
		"6-6,4-6",
		"2-6,4-8",
	}
	expected := 2
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestParseLine(t *testing.T) {
	tests := []struct {
		line                                                       string
		expectedStart1, expectedEnd1, expectedStart2, expectedEnd2 int
	}{
		{"2-4,6-8", 2, 4, 6, 8},
		{"2-3,4-5", 2, 3, 4, 5},
		{"5-7,7-9", 5, 7, 7, 9},
		{"2-8,3-7", 2, 8, 3, 7},
		{"6-6,4-6", 6, 6, 4, 6},
		{"2-6,4-8", 2, 6, 4, 8},
	}
	for _, test := range tests {
		t.Run(test.line, func(t *testing.T) {
			actualStart1, actualEnd1, actualStart2, actualEnd2, err := parse(test.line)

			assert.NoError(t, err)

			assert.Equal(t, test.expectedStart1, actualStart1)
			assert.Equal(t, test.expectedEnd1, actualEnd1)
			assert.Equal(t, test.expectedStart2, actualStart2)
			assert.Equal(t, test.expectedEnd2, actualEnd2)
		})
	}
}

func TestFullContains(t *testing.T) {
	tests := []struct {
		name                       string
		start1, end1, start2, end2 int
		expected                   bool
	}{
		{"2-4,6-8", 2, 4, 6, 8, false},
		{"2-3,4-5", 2, 3, 4, 5, false},
		{"5-7,7-9", 5, 7, 7, 9, false},
		{"2-8,3-7", 2, 8, 3, 7, true},
		{"6-6,4-6", 6, 6, 4, 6, true},
		{"2-6,4-8", 2, 6, 4, 8, false},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := fullContains(test.start1, test.end1, test.start2, test.end2)

			assert.Equal(t, test.expected, actual)
		})
	}
}
