package part1

import (
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	for _, line := range input {
		start1, end1, start2, end2, err := parse(line)
		if err != nil {
			return -1, err
		}

		if fullContains(start1, end1, start2, end2) {
			answer++
		}
	}

	return
}

func parse(line string) (start1, end1, start2, end2 int, err error) {
	parts := strings.Split(line, ",")

	parsePart := func(part string) (start, end int, err error) {
		parts := strings.Split(part, "-")
		start, err = strconv.Atoi(parts[0])
		if err != nil {
			return
		}
		end, err = strconv.Atoi(parts[1])
		return
	}

	start1, end1, err = parsePart(parts[0])
	if err != nil {
		return
	}

	start2, end2, err = parsePart(parts[1])

	return
}

func fullContains(start1, end1, start2, end2 int) bool {
	return (start1 <= start2 && end1 >= end2) ||
		(start2 <= start1 && end2 >= end1)
}
