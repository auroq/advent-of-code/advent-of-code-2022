package part2

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/utilities"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/utilities/datastructures"
	"strconv"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	for _, line := range input {
		start1, end1, start2, end2, err := parse(line)
		if err != nil {
			return -1, err
		}

		if overlap(start1, end1, start2, end2) {
			answer++
		}
	}

	return
}

func parse(line string) (start1, end1, start2, end2 int, err error) {
	parts := strings.Split(line, ",")

	parsePart := func(part string) (start, end int, err error) {
		parts := strings.Split(part, "-")
		start, err = strconv.Atoi(parts[0])
		if err != nil {
			return
		}
		end, err = strconv.Atoi(parts[1])
		return
	}

	start1, end1, err = parsePart(parts[0])
	if err != nil {
		return
	}

	start2, end2, err = parsePart(parts[1])

	return
}

func overlap(start1, end1, start2, end2 int) bool {
	nums1 := utilities.CreateRange(start1, end1+1)
	nums2 := utilities.CreateRange(start2, end2+1)

	nums := append(nums1, nums2...)

	numsSet := datastructures.NewIntSet(nums...)

	return numsSet.Count() != len(nums1)+len(nums2)
}
