package part1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"vJrwpWtwJgWrhcsFMMfFFhFp",
		"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
		"PmmdzqPrVvPwwTWBwg",
		"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
		"ttgJtRGJQctTZtZT",
		"CrZsJsPPZsGzwwsLwLmpwMDw",
	}
	expected := 157
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestParseLine(t *testing.T) {
	tests := []struct {
		line, expectedLeft, expectedRight string
	}{
		{"vJrwpWtwJgWrhcsFMMfFFhFp", "vJrwpWtwJgWr", "hcsFMMfFFhFp"},
		{"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", "jqHRNqRjqzjGDLGL", "rsFMfFZSrLrFZsSL"},
		{"PmmdzqPrVvPwwTWBwg", "PmmdzqPrV", "vPwwTWBwg"},
		{"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", "wMqvLMZHhHMvwLH", "jbvcjnnSBnvTQFn"},
		{"ttgJtRGJQctTZtZT", "ttgJtRGJ", "QctTZtZT"},
		{"CrZsJsPPZsGzwwsLwLmpwMDw", "CrZsJsPPZsGz", "wwsLwLmpwMDw"},
	}
	for _, test := range tests {
		t.Run(test.line, func(t *testing.T) {
			actualLeft, actualRight := parse(test.line)

			assert.Equal(t, test.expectedLeft, actualLeft)
			assert.Equal(t, test.expectedRight, actualRight)
		})
	}
}

func TestFindDuplicate(t *testing.T) {
	tests := []struct {
		left, right string
		expected    rune
	}{
		{"vJrwpWtwJgWr", "hcsFMMfFFhFp", 'p'},
		{"jqHRNqRjqzjGDLGL", "rsFMfFZSrLrFZsSL", 'L'},
		{"PmmdzqPrV", "vPwwTWBwg", 'P'},
		{"wMqvLMZHhHMvwLH", "jbvcjnnSBnvTQFn", 'v'},
		{"ttgJtRGJ", "QctTZtZT", 't'},
		{"CrZsJsPPZsGz", "wwsLwLmpwMDw", 's'},
	}
	for _, test := range tests {
		t.Run(test.left+"-"+test.right, func(t *testing.T) {
			actual := findDuplicate(test.left, test.right)

			assert.Equal(t, test.expected, actual)
		})
	}
}

func TestScoreRune(t *testing.T) {
	tests := []struct {
		char     rune
		expected int
	}{
		{'p', 16},
		{'L', 38},
		{'P', 42},
		{'v', 22},
		{'t', 20},
		{'s', 19},
	}
	for _, test := range tests {
		t.Run(string(test.char), func(t *testing.T) {
			actual := scoreRune(test.char)

			assert.Equal(t, test.expected, actual)
		})
	}
}
