package part1

import (
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	for _, line := range input {
		left, right := parse(line)
		duplicate := findDuplicate(left, right)
		answer += scoreRune(duplicate)
	}

	return
}

func parse(line string) (left, right string) {
	center := len(line) / 2
	return line[:center], line[center:]
}

func findDuplicate(left, right string) rune {
	for _, l := range left {
		if strings.ContainsRune(right, l) {
			return l
		}
	}

	return -1
}

func scoreRune(char rune) int {
	asciiVal := int(char)
	// If lowercase
	if asciiVal > 96 {
		return asciiVal - 96
	}

	// Add offset because uppercase comes after lowercase in this problem
	return asciiVal - 64 + 26
}
