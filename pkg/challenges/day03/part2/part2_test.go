package part2

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"vJrwpWtwJgWrhcsFMMfFFhFp",
		"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
		"PmmdzqPrVvPwwTWBwg",
		"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
		"ttgJtRGJQctTZtZT",
		"CrZsJsPPZsGzwwsLwLmpwMDw",
	}
	expected := 70
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestFindDuplicate(t *testing.T) {
	tests := []struct {
		first, second, third string
		expected             rune
	}{
		{
			"vJrwpWtwJgWrhcsFMMfFFhFp",
			"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
			"PmmdzqPrVvPwwTWBwg",
			'r',
		},
		{
			"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
			"ttgJtRGJQctTZtZT",
			"CrZsJsPPZsGzwwsLwLmpwMDw",
			'Z',
		},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("%s-%s-%s", test.first, test.second, test.third), func(t *testing.T) {
			actual := findDuplicate(test.first, test.second, test.third)

			assert.Equal(t, test.expected, actual)
		})
	}
}

func TestScoreRune(t *testing.T) {
	tests := []struct {
		char     rune
		expected int
	}{
		{'p', 16},
		{'L', 38},
		{'P', 42},
		{'v', 22},
		{'t', 20},
		{'s', 19},
	}
	for _, test := range tests {
		t.Run(string(test.char), func(t *testing.T) {
			actual := scoreRune(test.char)

			assert.Equal(t, test.expected, actual)
		})
	}
}
