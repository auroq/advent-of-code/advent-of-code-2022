package part2

import (
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	for i := 0; i < len(input); i += 3 {
		first := input[i]
		second := input[i+1]
		third := input[i+2]
		duplicate := findDuplicate(first, second, third)
		answer += scoreRune(duplicate)
	}

	return
}

func findDuplicate(first, second, third string) rune {
	for _, r := range first {
		if strings.ContainsRune(second, r) && strings.ContainsRune(third, r) {
			return r
		}
	}

	return -1
}

func scoreRune(char rune) int {
	asciiVal := int(char)
	// If lowercase
	if asciiVal > 96 {
		return asciiVal - 96
	}

	// Add offset because uppercase comes after lowercase in this problem
	return asciiVal - 64 + 26
}
