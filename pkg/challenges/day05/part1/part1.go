package part1

import (
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/utilities/datastructures"
	"strconv"
	"strings"
)

type Part1 struct{}

type instruction struct {
	count int
	from  int
	to    int
}

func newInstruction(count, from, to int) instruction {
	return instruction{count, from, to}
}

func (part Part1) Run(input []string) (answer int, err error) {
	var stackLines []string
	var i int
	var line string
	for i, line = range input {
		if strings.TrimSpace(line) == "" {
			break
		}
		stackLines = append(stackLines, line)
	}
	instructionLines := input[i+1:]

	stacks, err := parseStacks(stackLines)
	if err != nil {
		return
	}
	instructions, err := parseInstructionLines(instructionLines)
	if err != nil {
		return
	}

	for _, inst := range instructions {
		stacks = move(stacks, inst)
	}

	answerStr := ""
	for _, stack := range stacks {
		answerStr = answerStr + stack.Top().(string)
	}
	fmt.Println(answerStr)

	return -2, nil
}

func move(stacks []*datastructures.Stack, inst instruction) []*datastructures.Stack {
	for i := 0; i < inst.count; i++ {
		val := stacks[inst.from-1].Pop()
		stacks[inst.to-1].Push(val)
	}

	return stacks
}

func parseStacks(input []string) ([]*datastructures.Stack, error) {
	lastLine := strings.TrimSpace(input[len(input)-1])
	nums := strings.Split(lastLine, " ")
	lastNum, err := strconv.Atoi(nums[len(nums)-1])
	var stacks []*datastructures.Stack
	if err != nil {
		return nil, err
	}
	for i := 0; i < lastNum; i++ {
		stacks = append(stacks, datastructures.NewStack())
	}

	// -2 to ignore line with column numbers
	for lineNum := len(input) - 2; lineNum >= 0; lineNum-- {
		line := input[lineNum]
		for i := 0; i < len(line); i += 4 {
			if line[i] == '[' {
				val := string(line[i+1])
				stacks[i/4].Push(val)
			}
		}
	}

	return stacks, nil
}

func parseInstructionLines(lines []string) ([]instruction, error) {
	var instructions []instruction
	for _, line := range lines {
		inst, err := parseInstructionLine(line)
		if err != nil {
			return nil, err
		}
		instructions = append(instructions, inst)
	}

	return instructions, nil
}
func parseInstructionLine(line string) (instruction, error) {
	split := strings.Split(line, " ")
	count, err := strconv.Atoi(split[1])
	if err != nil {
		return instruction{}, err
	}
	from, err := strconv.Atoi(split[3])
	if err != nil {
		return instruction{}, err
	}
	to, err := strconv.Atoi(split[5])
	if err != nil {
		return instruction{}, err
	}

	return newInstruction(count, from, to), nil
}
