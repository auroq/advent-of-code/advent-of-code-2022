package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/utilities/datastructures"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"    [D]",
		"[N] [C]",
		"[Z] [M] [P]",
		" 1   2   3 ",
		"",
		"move 1 from 2 to 1",
		"move 3 from 1 to 3",
		"move 2 from 2 to 1",
		"move 1 from 1 to 2",
	}
	expected := -2
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestParseInstructionLine(t *testing.T) {
	tests := []struct {
		line     string
		expected instruction
	}{
		{"move 1 from 2 to 1", newInstruction(1, 2, 1)},
		{"move 3 from 1 to 3", newInstruction(3, 1, 3)},
		{"move 2 from 2 to 1", newInstruction(2, 2, 1)},
		{"move 1 from 1 to 2", newInstruction(1, 1, 2)},
	}
	for _, test := range tests {
		t.Run(test.line, func(t *testing.T) {
			actual, err := parseInstructionLine(test.line)

			assert.NoError(t, err)

			assert.Equal(t, test.expected, actual)
		})
	}
}

func TestParseStacks(t *testing.T) {
	var inputs = []string{
		"    [D]",
		"[N] [C]",
		"[Z] [M] [P]",
		" 1   2   3 ",
	}
	expectedStacks := []datastructures.Stack{
		*datastructures.NewStringStackFromBottomToTop([]string{"Z", "N"}),
		*datastructures.NewStringStackFromBottomToTop([]string{"M", "C", "D"}),
		*datastructures.NewStringStackFromBottomToTop([]string{"P"}),
	}

	actualStacks, err := parseStacks(inputs)

	assert.NoError(t, err)

	assert.Equal(t, len(expectedStacks), len(actualStacks))
	for i, expectedStack := range expectedStacks {
		assert.Equal(t, expectedStack, *actualStacks[i])
	}
}

func TestMove(t *testing.T) {
	beforeStacks := []*datastructures.Stack{
		datastructures.NewStringStackFromBottomToTop([]string{"Z", "N"}),
		datastructures.NewStringStackFromBottomToTop([]string{"M", "C", "D"}),
		datastructures.NewStringStackFromBottomToTop([]string{"P"}),
	}
	expectedStacks := []datastructures.Stack{
		*datastructures.NewStringStackFromBottomToTop([]string{"Z", "N"}),
		*datastructures.NewStringStackFromBottomToTop([]string{"M"}),
		*datastructures.NewStringStackFromBottomToTop([]string{"P", "D", "C"}),
	}

	inst := newInstruction(2, 2, 3)

	actualStacks := move(beforeStacks, inst)

	for i, expectedStack := range expectedStacks {
		assert.Equal(t, expectedStack, *actualStacks[i])
	}
}
