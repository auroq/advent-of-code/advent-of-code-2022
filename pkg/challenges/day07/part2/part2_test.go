package part2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"$ cd /",
		"$ ls",
		"dir a",
		"14848514 b.txt",
		"8504156 c.dat",
		"dir d",
		"$ cd a",
		"$ ls",
		"dir e",
		"29116 f",
		"2557 g",
		"62596 h.lst",
		"$ cd e",
		"$ ls",
		"584 i",
		"$ cd ..",
		"$ cd ..",
		"$ cd d",
		"$ ls",
		"4060174 j",
		"8033020 d.log",
		"5626152 d.ext",
		"7214296 k",
	}
	expected := 24933642
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestSize(t *testing.T) {
	root := NewDirectory(nil)
	a := root.AddSubDir("a")
	root.AddFile("b", 14848514)
	root.AddFile("c", 8504156)

	d := root.AddSubDir("d")
	d.AddFile("j", 4060174)
	d.AddFile("d.log", 8033020)
	d.AddFile("d.ext", 5626152)
	d.AddFile("k", 7214296)

	e := a.AddSubDir("e")
	a.AddFile("f", 29116)
	a.AddFile("g", 2557)
	a.AddFile("h", 62596)
	e.AddFile("i", 584)

	tests := []struct {
		name     string
		dir      *Directory
		expected int
	}{
		{"a", a, 94853},
		{"d", d, 24933642},
		{"e", e, 584},
		{"/", root, 48381165},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := test.dir.Size()

			assert.Equal(t, test.expected, actual)
		})
	}
}
