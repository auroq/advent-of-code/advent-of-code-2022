package part1

import (
	"strconv"
	"strings"
)

type Part1 struct{}

type Directory struct {
	files   map[string]*File
	subDirs map[string]*Directory
	parent  *Directory
}

type File struct {
	name   string
	size   int
	parent *Directory
}

func (part Part1) Run(input []string) (answer int, err error) {
	input = input[1:]
	var allDirectories []*Directory
	currentDir := NewDirectory(nil)
	allDirectories = append(allDirectories, currentDir)

	for _, line := range input {
		parts := strings.Split(line, " ")
		switch parts[0] {
		case "$":
			switch parts[1] {
			case "cd":
				if parts[2] == ".." {
					currentDir = currentDir.CdDotDot()
				} else {
					currentDir = currentDir.Cd(parts[2])
				}
			case "ls":
				continue
			}
		case "dir":
			newDir := currentDir.AddSubDir(parts[1])
			allDirectories = append(allDirectories, newDir)
		default: // file is listed on line
			fileSize, err := strconv.Atoi(parts[0])
			if err != nil {
				return -1, err
			}
			currentDir.AddFile(parts[1], fileSize)
		}
	}

	for _, dir := range allDirectories {
		if size := dir.Size(); size <= 100000 {
			answer += size
		}
	}

	return
}

func NewFile(name string, size int, parent *Directory) *File {
	return &File{
		name:   name,
		size:   size,
		parent: parent,
	}
}

func NewDirectory(parent *Directory) *Directory {
	return &Directory{
		files:   map[string]*File{},
		subDirs: map[string]*Directory{},
		parent:  parent,
	}
}

func (dir *Directory) Cd(name string) *Directory {
	if subDir, ok := dir.subDirs[name]; ok {
		return subDir
	}

	return nil
}

func (dir *Directory) CdDotDot() *Directory {
	return dir.parent
}

func (dir *Directory) AddSubDir(name string) *Directory {
	newDir := NewDirectory(dir)
	dir.subDirs[name] = newDir
	return newDir
}

func (dir *Directory) AddFile(name string, size int) {
	dir.files[name] = NewFile(name, size, dir)
}

func (dir *Directory) Size() int {
	size := 0

	for _, file := range dir.files {
		size += file.size
	}
	for _, subDir := range dir.subDirs {
		size += subDir.Size()
	}

	return size
}
