package part1

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/utilities/datastructures"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	answer = findMarker(input[0])
	return
}

func findMarker(dataStream string) int {
	for i := 4; i < len(dataStream); i += 1 {
		marker := dataStream[i-4 : i]
		stringSlice := strings.Split(marker, "")
		markerSet := datastructures.NewStringSet(stringSlice...)
		if markerSet.Count() == 4 {
			return i
		}
	}

	return -1
}
