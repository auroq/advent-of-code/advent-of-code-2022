package part1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"mjqjpqmgbljsphdztnvjfqwrcgsmlb",
	}
	expected := 7
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestFindMarker(t *testing.T) {
	tests := []struct {
		line     string
		expected int
	}{
		{"bvwbjplbgvbhsrlpgdmjqwftvncz", 5},
		{"nppdvjthqldpwncqszvftbrmjlhg", 6},
		{"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10},
		{"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11},
	}
	for _, test := range tests {
		t.Run(test.line, func(t *testing.T) {
			actual := findMarker(test.line)

			assert.Equal(t, test.expected, actual)
		})
	}
}
