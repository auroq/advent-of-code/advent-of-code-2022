package part2

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/utilities/datastructures"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	answer = findMarker(input[0], 14)
	return
}

func findMarker(dataStream string, numDistinct int) int {
	for i := numDistinct; i < len(dataStream); i += 1 {
		marker := dataStream[i-numDistinct : i]
		stringSlice := strings.Split(marker, "")
		markerSet := datastructures.NewStringSet(stringSlice...)
		if markerSet.Count() == numDistinct {
			return i
		}
	}

	return -1
}
