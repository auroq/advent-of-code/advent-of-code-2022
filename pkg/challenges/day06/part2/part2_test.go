package part2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"mjqjpqmgbljsphdztnvjfqwrcgsmlb",
	}
	expected := 19
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestFindMarker(t *testing.T) {
	tests := []struct {
		line     string
		expected int
	}{
		{"bvwbjplbgvbhsrlpgdmjqwftvncz", 23},
		{"nppdvjthqldpwncqszvftbrmjlhg", 23},
		{"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29},
		{"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26},
	}
	for _, test := range tests {
		t.Run(test.line, func(t *testing.T) {
			actual := findMarker(test.line, 14)

			assert.Equal(t, test.expected, actual)
		})
	}
}
