package part2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"1000",
		"2000",
		"3000",
		"",
		"4000",
		"",
		"5000",
		"6000",
		"",
		"7000",
		"8000",
		"9000",
		"",
		"10000",
	}
	expected := 45000
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestLargestIndex(t *testing.T) {
	input := []int{
		6000,
		4000,
		11000,
		24000,
		10000,
	}
	expected := 3
	actual := largestIndex(input)
	assert.Equal(t, expected, actual)
}
