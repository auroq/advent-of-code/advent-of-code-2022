package part2

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2022/pkg/utilities"
	"math"
	"strconv"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	var elves []int
	sum := 0
	for _, line := range input {
		if line == "" {
			elves = append(elves, sum)
			sum = 0
		} else {
			calories, err := strconv.Atoi(line)
			if err != nil {
				return -1, err
			}
			sum += calories
		}
	}
	elves = append(elves, sum)

	answer = 0
	for i := 0; i < 3; i++ {
		item := utilities.Max(elves...)
		elves = utilities.Remove(elves, item)
		answer += item
	}

	return
}

func largestIndex(input []int) int {
	index := -1
	value := math.MinInt
	for i, val := range input {
		if val > value {
			index = i
			value = val
		}
	}

	return index
}
