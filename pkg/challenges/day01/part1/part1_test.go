package part1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"1000",
		"2000",
		"3000",
		"",
		"4000",
		"",
		"5000",
		"6000",
		"",
		"7000",
		"8000",
		"9000",
		"",
		"10000",
	}
	expected := 24000
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestLargestIndex(t *testing.T) {
	input := []int{
		6000,
		4000,
		11000,
		24000,
		10000,
	}
	expected := 3
	actual := largestIndex(input)
	assert.Equal(t, expected, actual)
}
