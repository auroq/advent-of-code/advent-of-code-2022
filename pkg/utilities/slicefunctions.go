package utilities

func CreateRange(startInclusive, endExclusive int) (r []int) {
	for i := startInclusive; i < endExclusive; i++ {
		r = append(r, i)
	}

	return
}

func Remove[T comparable](l []T, item T) []T {
	for i, other := range l {
		if other == item {
			return append(l[:i], l[i+1:]...)
		}
	}
	return l
}
