package datastructures

type Set struct {
	values  map[interface{}]int64
	counter int64
}

func NewSet(items ...interface{}) *Set {
	set := &Set{
		values:  map[interface{}]int64{},
		counter: 0,
	}
	for _, item := range items {
		set.Add(item)
	}

	return set
}

func NewIntSet(items ...int) *Set {
	set := NewSet()
	for _, item := range items {
		set.Add(item)
	}
	return set
}

func NewStringSet(items ...string) *Set {
	set := NewSet()
	for _, item := range items {
		set.Add(item)
	}
	return set
}

func NewRuneSet(items ...rune) *Set {
	set := NewSet()
	for _, item := range items {
		set.Add(item)
	}
	return set
}

func NewRuneSetFromString(items string) *Set {
	set := NewSet()
	for _, item := range []rune(items) {
		set.Add(item)
	}
	return set
}

func (set Set) Count() int {
	return len(set.values)
}

func (set *Set) Add(item interface{}) {
	set.counter++
	set.values[item] = set.counter
}

func (set Set) Contains(item interface{}) bool {
	for value := range set.values {
		if value == item {
			return true
		}
	}

	return false
}

func (set Set) Remove(item interface{}) {
	delete(set.values, item)
}

func (set Set) ToSlice() (values []interface{}) {
	for key := range set.values {
		values = append(values, key)
	}

	return
}

func (set Set) ToStringSlice() (values []string) {
	for key := range set.values {
		values = append(values, key.(string))
	}

	return
}

func (set Set) Copy() (newSet *Set) {
	newSet = NewSet()
	for k, v := range set.values {
		newSet.values[k] = v
	}
	return newSet
}

func (set Set) ToOrderedSlice() (values []interface{}) {
	fullSize := make([]interface{}, set.counter+1)
	for key, index := range set.values {
		fullSize[index] = key
	}
	for _, value := range fullSize {
		if value != nil {
			values = append(values, value)
		}
	}

	return
}

func (set Set) Equals(other Set) bool {
	for k, v := range set.values {
		if otherV, ok := other.values[k]; !ok || otherV != v {
			return false
		}
	}

	return true
}
