package utilities

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRemove(t *testing.T) {
	tests := []struct {
		name              string
		toRemove          int
		initial, expected []int
	}{
		{
			"first item",
			4,
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546, 345, 345},
			[]int{6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546, 345, 345},
		},
		{
			"last item",
			344,
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546, 344},
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546},
		},
		{
			"duplicated item",
			3,
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546, 345, 345},
			[]int{4, 6, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546, 345, 345},
		},
		{
			"duplicated item near end",
			345,
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546, 345, 345},
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 546, 345, 345},
		},
		{
			"unique item",
			54,
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546, 345, 345},
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 3, 3, 345, 546, 345, 345},
		},
		{
			"missing item",
			-1,
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546, 345, 345},
			[]int{4, 6, 3, 5, 3, 3, 5, 4, 6, 54, 3, 3, 345, 546, 345, 345},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			expected := test.expected
			actual := Remove(test.initial, test.toRemove)

			assert.Equal(t, expected, actual)
		})
	}
}
